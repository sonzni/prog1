package anagram;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Anagram {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj dwa słowa a sprzwdzę czy są anagramami");
        System.out.println("Podaj pierwsze słowo:");
        String firstWord = scanner.nextLine();
        System.out.println("Podaj drugie słowo:");
        String secondWord = scanner.nextLine();


        char[] firstWordCharacters = firstWord.toCharArray();
        char[] secondWordCharacters = secondWord.toCharArray();

        Arrays.sort(firstWordCharacters);
        Arrays.sort(secondWordCharacters);

        boolean areEqual = Arrays.equals(firstWordCharacters, secondWordCharacters);

        if(areEqual){
            System.out.println("Podane słowa są anagramami");
        } else{
            System.out.println("Podane słowa nie są anagramami");
        }

        //for(char slowo: chars){
           // System.out.println(slowo);

    }
}

