package adventOfCode;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day1 {
    public static void main(String[] args) throws IOException {
        //wzor na obl paliwa:    (mass / 3 -> wynik zaokaglic) - 2    ---> to nam da PALIWO
        // dla każdego wpisu z pliku liczymy paliwo,
        //następnie sumujemy wszystko
        //1. wczytanie pliku i wypisanie wartości na konsolę

        Stream<String> inputValues = Files.lines(Path.of("src/main/resources/adventofcode/day1input"));

        List<String> listOfModuleMass = inputValues.collect(Collectors.toList());

        int sum = 0;
        for(String mass : listOfModuleMass) {
            int requireFuel = calculateFuel(Integer.parseInt(mass));
            sum += requireFuel;
                System.out.println("Fuel needed for mass"+ mass + "--->" + requireFuel);
        }
        System.out.println("Sum of required fuel"+ sum);
    }

    public static int calculateFuel(int mass){
        int fuel = (mass / 3)-2;
        if(fuel <=0) {
            return 0;
        }else{
            return fuel + calculateFuel(fuel);
        }
    }
    }