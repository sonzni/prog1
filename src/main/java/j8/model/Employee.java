package j8.model;

public class Employee {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public ConctractType getType() {
        return type;
    }

    public void setType(ConctractType type) {
        this.type = type;
    }

    String name;
    String lastName;
    int age;
    String profession;
    double salary;
    ConctractType type;

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", profession='" + profession + '\'' +
                ", salary=" + salary +
                ", type=" + type +
                '}';
    }

    public Employee(String name, String lastName, int age, String profession, double salary, ConctractType type) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.profession = profession;
        this.salary = salary;
        this.type = type;
    }




}
