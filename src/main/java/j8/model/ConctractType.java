package j8.model;

public enum ConctractType {
    F("Full"),
    H("Half");

    String description;

    ConctractType(String description) {
        this.description = description;
    }

}
