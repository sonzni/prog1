package j8.fi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class PlayWithConsumer {
    public static void main(String[] args) {
        Consumer<String> consumer = (String t) -> System.out.println(t); //zapis lambdy
        //Consumer<String> consumer = System.out::println;     //ustaw kursor na Consumer i CTRL + MYSZKA - wchodzimy do klasy Consumer // zapis skrocony lambdy
        consumer.accept("Jakiś string");


       // Consumer<List> imiona = (List) -> System.out.println(List.clear();)
       // imiona.accept(List.of());
        //czyscimy liste:

        List<String> stringList = new ArrayList<>(Arrays.asList("Kasia,", "ola", "Basia"));

        Consumer<List<String>> listConsumer = (List<String> e) -> e.clear();
        listConsumer.accept(stringList);
        System.out.println(stringList.size());

        //dodawanie consumerow

        MyConsumer<List<String>> addConsumer = (List<String>t) -> t.add("Test");
        MyConsumer<List<String>> secondAddConsumer = (List<String>t) -> t.add("hello");

        MyConsumer<List<String>> groupingConsumer = addConsumer.andThen(secondAddConsumer);

        groupingConsumer.accept(stringList);

        System.out.println(stringList);






    }


}
