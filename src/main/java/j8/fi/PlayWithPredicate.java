package j8.fi;

import java.util.function.Predicate;

public class PlayWithPredicate {
    public static void main(String[] args) {

        Predicate<String> checkA = (String t) -> t.contains("a");


        System.out.println(checkA.test("Ania"));
        System.out.println(checkA.test("Yeti"));

        Predicate<String> check2 = (String t) -> t.isEmpty();              //sprawdzamy czy jest pusty
        System.out.println(check2.test("coś"));

        Predicate<String> hasAEndEmptyCheck = checkA.and(check2);
        hasAEndEmptyCheck.test("test");
    }
}