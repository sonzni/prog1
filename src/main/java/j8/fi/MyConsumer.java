package j8.fi;

import java.util.Objects;

@FunctionalInterface
public interface MyConsumer<T> {

    void accept(T t);                                  //stworzylismy funkcyjny interfejs (u gory dodaj @Functional..)

    default MyConsumer<T> andThen(MyConsumer<T>other){           //tworzymy wlasna metode o nazwie andThen(): implementujemy teraz metode accept
        Objects.requireNonNull(other);
        return (T t) -> {
                this.accept(t);
                other.accept(t);
     };
    }
}
