package j8.stream;

import j8.model.ConctractType;
import j8.model.Employee;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLOutput;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;


public class PlayWithStream {
    public static void main(String[] args) {

        List<Employee> employees = FileUtils.load(Paths.get("src/main/resources/j8/employees"));

        //  ex1(employees);     //metoda ex1 oznacza zadanie pierwsze
        //  ex2(employees);      //zad drugie
        //  ex3(employees);
        //  ex4(employees);
        //   ex5(employees);
        //  ex6(employees);
        //  ex7(employees);
        //  ex8(employees);
        // ex9(employees);
        // ex10(employees);
        // ex11(employees);
        //  ex12(employees);
        ex13(employees);
        ex14(employees);
        ex14a(employees);
       // ex14c(employees);
        ex15(employees);
    }

    private static void hy(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getSalary() > 2500 && e.getSalary() < 3199)
                .forEach(System.out::println);
    }

    private static void ex1(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getSalary() > 2500 && e.getSalary() < 3199)
                .forEach(System.out::println);
    }

    private static void ex2(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getAge() % 2 == 0)
                .forEach(System.out::println);
    }

    private static void ex3(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getLastName().endsWith("ska"))
                .filter(e -> e.getType().equals(ConctractType.F))
                .forEach(System.out::println);
    }

    private static void ex4(List<Employee> employees) {
        employees.stream()
                .map(Employee::getName)           //zamiast lambdy uzylismy metod references a z lambdą wyglądałoby tak: e-> e.getName()
                .forEach(System.out::println);
    }

    private static void ex5(List<Employee> employees) {
        employees.stream()
                .map(e -> e.getLastName())      //najwpierw wjezdza lista nazwisk
                .map(e -> e.substring(e.length() - 3)) //odcinamy 3 ostatnie literki z nazwiska
                .map(e -> (e.endsWith("ski") || e.endsWith("ska"))
                        ? e.toUpperCase() : e)                                   //jezeli boolean zwroci nam true wykona się to co po znaku zapytania, jesli zwroci false zwroci to, co mamy po dwukropku(czyli daje "e" wiec nie zmienione
                .forEach(System.out::println);
    }

    private static void ex6(List<Employee> employees) {
        employees.stream()
                .map(e -> e.getProfession())
                .map(e -> e.toUpperCase())
                .forEach(System.out::println);
    }

    private static void ex7(List<Employee> employees) {
        boolean kowalski = employees.stream()
                .anyMatch(e -> e.getLastName().equals("Kowalska"));   //stringi ZAWSZE powównujemy przez equals a nie przez "=="
        System.out.println(kowalski);
    }

    private static void ex8(List<Employee> employees) {
        Map<String, List<Double>> collect = employees.stream()
                .collect(
                        Collectors.groupingBy(
                                Employee::getLastName,
                                Collectors.mapping(
                                        e -> e.getSalary() * 1.12, toList())));
        System.out.println(collect);
    }

    private static void ex9(List<Employee> employees) {
        employees.stream()
                .filter(e -> e.getName().charAt(2) == 'a')
                .filter(e -> e.getLastName().charAt(4) == 'b')
                .forEach(System.out::println);     //foreach nic nie zwraca.Jak chcemy wzrocic stringa to daj sout
    }

    private static void ex10(List<Employee> employees) {
        employees.stream()
                .sorted(
                        (person1, person2) -> {
                            int lastNameComparator =
                                    person1.getLastName().compareTo(person2.getLastName());
                            if (lastNameComparator != 0) {
                                return lastNameComparator;
                            }
                            return person1.getName()
                                    .compareTo(person2.getName());
                        })
                //Comparator.comparing(Employee:: getLastName).thenComparing(Employee::getName)
                .forEach(System.out::println);
    }

    private static void ex11(List<Employee> employees) {
        String collect = employees.stream()
                .map(e -> e.getLastName())
                .collect(Collectors.joining(","));    //oddzielamy przecinkiem wszystkie nazwiska
        System.out.println(collect);
    }

    private static void ex12(List<Employee> employees) {
        Map<Boolean, Long> collect = employees.stream()
                .collect(
                        Collectors.partitioningBy(
                                e -> e.getSalary() > 5000,             //wszedzie gdzie zarobki byly mniejsze niz 5000 zl mamy false
                                Collectors.counting()
                        ));

        System.out.println(collect);
    }

    private static void ex13(List<Employee> employees) {
        Map<String, Double> collect = employees.stream()                                                  //grouping by tworzy jakiby liste duplikatow
                .collect(
                        groupingBy(
                                e -> e.getType().name(),
                                averagingDouble(
                                        e -> e.getSalary()
                                        // Collectors.toList()
                                )
                        ));
        System.out.println(collect);
    }

    private static void ex14(List<Employee> employees) {
        Map<String, List<Employee>> collect = employees.stream()
                .collect(
                        groupingBy(
                                e -> e.getName()
                        )
                );
        System.out.println(collect);
    }

    private static void ex14a(List<Employee> employees) {
        Map<String, Long> collect = employees.stream()
                .collect(
                        groupingBy(
                                e -> e.getName(),
                                Collectors.counting()     //zlicza elementy i dostajemy longa

                        )
                );
        System.out.println(collect);
    }
    private static void ex15(List<Employee> employees) {
        Optional<Employee> collect = employees.stream()             //optional daje sie po to, by nie wyrzucilo nam exception
                .collect(
                Collectors.maxBy(
                        Comparator.comparing(
                                e-> e.getSalary())
                ));
        System.out.println(collect);

    }



        //   private static void ex14c(List<Employee> employees) {
      //  Map<String, Long> collect = employees.stream()
        //        .collect(
              //          groupingBy(                                       //groupingby to inaczej: stworz mi mape
             //                   e -> e.getName(),
                    //             filtering(
                    //                     e -> e.getName().endsWith("a")),
                      //          counting()
                 //       )

            //    );
     //   System.out.println(collect);
   // }
}
