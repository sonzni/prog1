### Consumer
+ Napisz Consumer który czyści listę stringów

### Predicate 
+ Napisz Predicate który sprawdza czy podany ciąg znaków jest pusty
+ Napisz Predykat który sprawdza czy podany ciąg znaków nie jest pusty i nie jest nullem

------

1. Stwórz klasę Employee z polami : 
  - firstName
  - lastName
  - age
  - profession
  - salary
  - contractType

Następnie zbudujmy listę pracowników z która będziemy pracowali później : 

        List<Employee> employees = List.of(
            new Employee(.....),
            new Employee(.....),
            new Employee(.....)
        );

### Map 

            employees.stream() 
                     .map(...)
                     .forEach(System.out::println)
                     
+ wypisz imiona wszystkich pracowników
+ wypisz profesje wszystkich pracownikow z dużych liter

### Filter 

       
              employees.stream()
                     .map(...)
                     .filter(....)
                     .forEach(System.out::println)
                


+ wypisz osoby o wieku parzystym
+ wypisz osoby których nazwisko kończy się na ska i pracują na cały etat

### flatMap

### Collectors 

        stream()
            .filter()
            .map()
            .collect(**Collector**)
        

+ toList() - dodaj do listy wszystkich w wieku powyżej 30 lat zarabiających powyżej 2000zł
 (ma powstać nowa list)
 
        stream()
             .collect(toMap(...))
 
+ toMap() - stwórz mapę gdzie kluczem jest nazwisko a wartościa pensja

       stream()
           .map()
           .collect(...) 

+ joining() - wypisz wszystkie imiona oddzielone | 

+ partitioningBy() - użyj w celu podzielenia pracowników na FULL/HALF 

+ groupingBy()

        groupingBy(Function<T,R) -> Collector
        groupingBy(Function<T,R, Collector) -> Collector
        
    + Stwórz mapę gdzie kluczem będzie Imię a wartościa List<Employee> 
    + Stwórz mapę gdzie kluczem będzie Profesja a wartościa List<Double> jako salary
        (użyj funkcji mapping() jako drugi kolektor)
        
            stream()
               .collect(groupingBy(...., mapping()))
 
    + Stórz mapę której kluczem będzie imie a wartościa ilość jego wystąpień (groupingBy() + couting())
    + Pogrupuj imiona po ich długości 
    
+ collectingAndThen()

        groupingBy and mapping (Function, Collector)
        collectingAndThen (Collector, Function)
        
        stream()
             .collect(grupingBy(.... , collectingAndThen()))
        
  + wróc do poprzedniego przykładu i postaraj się przerobić ilość wystąpień z Longa na Integer
  używając groupingBy() i collectingAndThen()
  
+ maxBy()
    + znajdz osobę najlepiej zarabiająca
    + znajdz imię najstarszej osoby (użyj collectingAndThen oraz maxBy)
    
+ filtering()
   + Stwórz Mapę której kluczem będzie profesja a kluczem lista wszystkich zarobkow mniejszych niz 2000
    (użyj grupingBy, mapping, filtering)
 