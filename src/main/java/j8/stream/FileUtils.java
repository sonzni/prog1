package j8.stream;

import j8.model.ConctractType;
import j8.model.Employee;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileUtils {

    static List<Employee> load(Path path) {
        try {
            Stream<String> linesOfCsvFile = Files.lines(path);                         //wczytujemy plik txt z employees

            return linesOfCsvFile.skip(1)              ////nie chcemy linii pierwszej z l=pliku txt z IMIE NAZWISKO itd.
                    .map(e -> e.split(","))
                     //stream stringów
                    .map(FileUtils::mapToObject)  //uzycie method elements
                   // .forEach(System.out::println)
                   //.forEach(employee -> System.out.println(employee));  //bez uzycia method reference linijka wyzej bedzie wygladala jak ta obok
                .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
    public static Employee mapToObject(String[] empArray){
        String firstName = empArray[0];
        String lastName = empArray[1];
        int age = Integer.parseInt(empArray[2]);
        String profession = empArray[3];
        Double salary = Double.parseDouble(empArray[4]);
        ConctractType contractType = ConctractType.valueOf(empArray[5]);

        return new Employee(firstName, lastName, age, profession, salary, contractType);

    }
}

