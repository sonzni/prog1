package factorial;

import org.w3c.dom.ls.LSOutput;

public class Factorial {
    public static void main(String[] args) {
        System.out.println(factorial(3));
    }

    // silnie oznaczamy: 4! -> 4 * 2 * 2 * 1
    // n! -> (n-1) * (n-2) * (n-3) * ..... *3 * 2 *1;
//  lub przedstawiamy tak:
// n! -> (n-1)!
    //5! -> 5* (5-1)!
    //5 * 4! -> 120
    // 4! -> 4 * (4 -1)!
    //4 * 3! -> 24
    // 3! -> 3 * 2! -> 6
    //2! -> 2

    static int factorial(int n) {
        if (n >= 1) {
            return n * factorial(n - 1);
        } else {
            return 1;
        }
    }

}

