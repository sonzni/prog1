package calculator;

import java.util.function.IntBinaryOperator;

public enum Calculator {
    ADD("Dodawanie", (x,y) -> x + y),
    SUBSTRACT("Odejmowanie", (x,y) -> x-y),
    DIVIDE("Dzielenie", (x,y) -> x/y);


    private String description;
    private IntBinaryOperator operator;

    Calculator(String description, IntBinaryOperator operator){
        this.operator = operator;
        this.description = description;

    }
    public int calculate(int a, int b){
        return operator.applyAsInt(a,b);
    }
}
