package calculator;

public class Main {

    public static void main(String[] args) {
        Example.RED.something();
        Example.GREEN.something();
        Example.BLUE.something();
        System.out.println(Example.RED.getDesc());
        System.out.println(Example.GREEN.getDesc());
        System.out.println(Example.BLUE.getDesc());


        Calculator.ADD.calculate(3,4);
        Calculator.DIVIDE.calculate(52,2);
        Calculator.SUBSTRACT.calculate(125, 14);
        System.out.println(Calculator.SUBSTRACT.calculate(125, 14));
        System.out.println(Calculator.DIVIDE.calculate(52,2));
        System.out.println(Calculator.ADD.calculate(3,4));

    }
}
