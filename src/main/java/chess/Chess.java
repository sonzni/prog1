package chess;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Chess {

    public static final String BLACK_FIELD = " \u25A0 ";
    public static final String WHITE_FIELD = " \u25A1 ";

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj długość szachownicy");

        int chess_lenght = scanner.nextInt();                                           //zatrzymuje program i wpisuję w konsolę jakąś wartość np 10 i ok
        System.out.println("Rysujesz szachownicę o długośći " + chess_lenght);

        for (int y = 0; y < chess_lenght; y++) {                       //petla pomocnicza, tylko po to zeby przechodzic do nowej linii (ma printLN)
            System.out.print("");
            for (int x = 0; x < chess_lenght; x++) {
                //tutaj rysujemy wiersze x:
                if ((x+y) %2 == 0) {                          //bez dodania "x+y" mielibysmy wszystkie linijki takie same
                    System.out.print(BLACK_FIELD);            //znak unikodowy -black, moze tez byc "*", unikod wstawia dany symbol
                } else {
                    System.out.print(WHITE_FIELD);                                           //print a nie println bo nie chcemy przenosic do nowej linii
                }
            }
            System.out.println("");    //tutaj przechodzimy do nowej linii Y
        }
    }
}


