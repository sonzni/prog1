package lock;

import java.util.Random;
import java.util.Scanner;

public class Lock {

    private int correctA;    //prawdziwe wartosci
    private int correctB;
    private int correctC;

    private int currentA;    //aktualna kombinacja zamka
    private int currentB;
    private int currentC;


    public Lock(int correctA, int correctB, int correctC) {
        this.currentA = this.correctA = correctA;
        this.currentB = this.correctB = correctB;
        this.currentC = this.correctC = correctC;


    }

    public void switchA(){
        currentA = (currentA + 1) % 10;  //w jednej linijce mozemy zapisac to samo co jest niżej czyli albo  if - else jak niżej
       // if (currentA == 9) {
         //   currentA = 0;
       // } else {
       //     currentA++;
       // }
    }

    public void switchB() {
        if (currentB == 9) {
            currentB = 0;
        } else {
            currentB++;
        }
    }
        public void switchC(){
        if (currentC == 9) {
            currentC = 0;
        } else {
            currentC++;
        }
    }

        boolean isOpen() {
            System.out.println("Sprawdzam czy zamek jest otwarty");
            return currentA == correctA &&
                    currentB == correctB &&
                    currentC == correctC;
        }


        public void shuffle () {                                               //zmienia kombinacje naszego zamka
        Random random = new Random();                               //tworze obiekt ktory bedzie liczbami losowymi
        currentA = random.nextInt(10);
        currentB = random.nextInt(10);
        currentC = random.nextInt(10);
        System.out.println("Po zmianie kombinacji -> " + currentA + " " + currentB + " " + currentC);
    }

        @Override
        public String toString () {
        return "Aktualna kombinacja zamka: " + currentA + " " + currentB + " " + currentC;
    }
    }

