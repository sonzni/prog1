package lock;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Podaj kombinację do zamknięcia zamka w formacie 1-2-4");
        Scanner scanner = new Scanner(System.in);

        String[] lockCombination = scanner.nextLine().split("-");

        Lock lock = new Lock(
                Integer.parseInt(lockCombination[0]),
                Integer.parseInt(lockCombination[1]),
                Integer.parseInt(lockCombination[2])
        );

        System.out.println("Tworzę zamek lock -->" + lock);
        lock.shuffle();
        System.out.println(lock);
        lock.switchA();
        lock.switchB();
        lock.switchC();
        System.out.println(lock);

        lock.isOpen();
        System.out.println(lock.isOpen());


        if(lock.isOpen()) {
            System.out.println("Zamek jest otwarty");
        }else{
                    System.out.println("Zamek jest zamknięty");
                }
            }
        }

