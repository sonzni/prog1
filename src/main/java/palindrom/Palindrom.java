package palindrom;

import java.util.Scanner;

public class Palindrom {
    public static void main(String[] args) {
        //KAJAK
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj słowo a sprzwdzę czy jest palindromem");

        String wordToCheck = scanner.nextLine();

        boolean isPalindrom = true;
        int start = 0;
        int end = wordToCheck.length() - 1;

        while (start <= end) {         //dopóki moje start jest mniejsze lub równe end to coś rób
            System.out.println("Checking equality again" + wordToCheck.charAt(start) + "vs" + wordToCheck.charAt(end));
            if (wordToCheck.charAt(start) != wordToCheck.charAt(end)) {    //K A J A K   charAt() -> K, potem chartAt() zwraca -> A.. sprawdza czy znak na poczatku jest różny od znaku na końcu
                isPalindrom = false;
                break;
            }
            start++;
            end--;
        }
        if (isPalindrom) {
            System.out.println("Podane słowo " + wordToCheck + " jest palindromem");
        } else {
            System.out.println("Słowo " + wordToCheck + " nie jest palindromem");
        }
    }
}